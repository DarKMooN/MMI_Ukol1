solution "Ukol1-Workspace"
	configurations {"Debug", "Release"}
	
	project "Ukol1"
		kind "WindowedApp"
		language "C++"
		targetdir "../bin"
		files {"../*.cpp", "../*.h"}
		
		buildoptions {"$(shell wx-config --cxxflags)", "--std=c++11"}
		linkoptions "$(shell wx-config --libs)" 
		
		configuration "Debug"
			defines "DEBUG"
			flags {"Symbols", "ExtraWarnings"}
			linkoptions "$(shell wx-config --debug=yes --libs core,base,propgrid --unicode=yes)"
			
		configuration "Release"
			defines "NDEBUG"
			flags "Optimize"
			linkoptions {"$(shell wx-config --debug=no --libs core,base,propgrid --unicode=yes)", "-s"}
			
		configuration "windows"
			flags "WinMain"
			files "../*.rc"
			resoptions "$(shell wx-config --rcflags)"
