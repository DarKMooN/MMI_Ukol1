##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=ukol1
ConfigurationName      :=Debug
WorkspacePath          := "/home/darkmoon/.codelite/MMI"
ProjectPath            := "/home/darkmoon/.codelite/MMI/ukol1"
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=Tomáš Juřena
Date                   :=03/06/15
CodeLitePath           :="/home/darkmoon/.codelite"
LinkerName             :=/usr/bin/g++ 
SharedObjectLinkerName :=/usr/bin/g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=$(PreprocessorSwitch)__WX__ 
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="ukol1.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  $(shell wx-config --debug=yes --libs core,base,propgrid --unicode=yes)
IncludePath            :=  $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++ 
CC       := /usr/bin/gcc 
CXXFLAGS :=  -g -O0 -std=c++11 -Wall $(shell wx-config --cxxflags --unicode=yes --debug=yes) $(Preprocessors)
CFLAGS   :=  -g -O0 -std=c++11 -Wall $(shell wx-config --cxxflags --unicode=yes --debug=yes) $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as 


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/main.cpp$(ObjectSuffix) $(IntermediateDirectory)/gui.cpp$(ObjectSuffix) $(IntermediateDirectory)/bitmapi.cpp$(ObjectSuffix) $(IntermediateDirectory)/bmp.cpp$(ObjectSuffix) $(IntermediateDirectory)/pcx.cpp$(ObjectSuffix) $(IntermediateDirectory)/steganography.cpp$(ObjectSuffix) $(IntermediateDirectory)/convolutionmatrixdlg.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

$(IntermediateDirectory)/.d:
	@test -d ./Debug || $(MakeDirCommand) ./Debug

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/main.cpp$(ObjectSuffix): main.cpp $(IntermediateDirectory)/main.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/darkmoon/.codelite/MMI/ukol1/main.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/main.cpp$(DependSuffix): main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/main.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/main.cpp$(DependSuffix) -MM "main.cpp"

$(IntermediateDirectory)/main.cpp$(PreprocessSuffix): main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/main.cpp$(PreprocessSuffix) "main.cpp"

$(IntermediateDirectory)/gui.cpp$(ObjectSuffix): gui.cpp $(IntermediateDirectory)/gui.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/darkmoon/.codelite/MMI/ukol1/gui.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/gui.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/gui.cpp$(DependSuffix): gui.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/gui.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/gui.cpp$(DependSuffix) -MM "gui.cpp"

$(IntermediateDirectory)/gui.cpp$(PreprocessSuffix): gui.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/gui.cpp$(PreprocessSuffix) "gui.cpp"

$(IntermediateDirectory)/bitmapi.cpp$(ObjectSuffix): bitmapi.cpp $(IntermediateDirectory)/bitmapi.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/darkmoon/.codelite/MMI/ukol1/bitmapi.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/bitmapi.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/bitmapi.cpp$(DependSuffix): bitmapi.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/bitmapi.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/bitmapi.cpp$(DependSuffix) -MM "bitmapi.cpp"

$(IntermediateDirectory)/bitmapi.cpp$(PreprocessSuffix): bitmapi.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/bitmapi.cpp$(PreprocessSuffix) "bitmapi.cpp"

$(IntermediateDirectory)/bmp.cpp$(ObjectSuffix): bmp.cpp $(IntermediateDirectory)/bmp.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/darkmoon/.codelite/MMI/ukol1/bmp.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/bmp.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/bmp.cpp$(DependSuffix): bmp.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/bmp.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/bmp.cpp$(DependSuffix) -MM "bmp.cpp"

$(IntermediateDirectory)/bmp.cpp$(PreprocessSuffix): bmp.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/bmp.cpp$(PreprocessSuffix) "bmp.cpp"

$(IntermediateDirectory)/pcx.cpp$(ObjectSuffix): pcx.cpp $(IntermediateDirectory)/pcx.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/darkmoon/.codelite/MMI/ukol1/pcx.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/pcx.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pcx.cpp$(DependSuffix): pcx.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pcx.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/pcx.cpp$(DependSuffix) -MM "pcx.cpp"

$(IntermediateDirectory)/pcx.cpp$(PreprocessSuffix): pcx.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pcx.cpp$(PreprocessSuffix) "pcx.cpp"

$(IntermediateDirectory)/steganography.cpp$(ObjectSuffix): steganography.cpp $(IntermediateDirectory)/steganography.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/darkmoon/.codelite/MMI/ukol1/steganography.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/steganography.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/steganography.cpp$(DependSuffix): steganography.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/steganography.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/steganography.cpp$(DependSuffix) -MM "steganography.cpp"

$(IntermediateDirectory)/steganography.cpp$(PreprocessSuffix): steganography.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/steganography.cpp$(PreprocessSuffix) "steganography.cpp"

$(IntermediateDirectory)/convolutionmatrixdlg.cpp$(ObjectSuffix): convolutionmatrixdlg.cpp $(IntermediateDirectory)/convolutionmatrixdlg.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/darkmoon/.codelite/MMI/ukol1/convolutionmatrixdlg.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/convolutionmatrixdlg.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/convolutionmatrixdlg.cpp$(DependSuffix): convolutionmatrixdlg.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/convolutionmatrixdlg.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/convolutionmatrixdlg.cpp$(DependSuffix) -MM "convolutionmatrixdlg.cpp"

$(IntermediateDirectory)/convolutionmatrixdlg.cpp$(PreprocessSuffix): convolutionmatrixdlg.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/convolutionmatrixdlg.cpp$(PreprocessSuffix) "convolutionmatrixdlg.cpp"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r ./Debug/


