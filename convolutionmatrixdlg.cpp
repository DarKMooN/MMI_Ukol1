#include "convolutionmatrixdlg.h"

ConvolutionMatrixDlg::ConvolutionMatrixDlg( wxWindow *parent ) : ConvolutionFrame( parent )
{
	SetWindowStyle( ( wxDEFAULT_FRAME_STYLE | wxTAB_TRAVERSAL ) ^ wxRESIZE_BORDER );
}

ConvolutionMatrixDlg::~ConvolutionMatrixDlg()
{
	Destroy();
}

void ConvolutionMatrixDlg::OnCancelClick( wxCommandEvent& event )
{
	Destroy();
}

void ConvolutionMatrixDlg::OnOkClick( wxCommandEvent& event )
{
	m_kernel00->GetValue().ToDouble( &_kernel->kernel[0] );
	m_kernel01->GetValue().ToDouble( &_kernel->kernel[1] );
	m_kernel02->GetValue().ToDouble( &_kernel->kernel[2] );
	m_kernel10->GetValue().ToDouble( &_kernel->kernel[3] );
	m_kernel11->GetValue().ToDouble( &_kernel->kernel[4] );
	m_kernel12->GetValue().ToDouble( &_kernel->kernel[5] );
	m_kernel20->GetValue().ToDouble( &_kernel->kernel[6] );
	m_kernel21->GetValue().ToDouble( &_kernel->kernel[7] );
	m_kernel22->GetValue().ToDouble( &_kernel->kernel[8] );
	m_divider->GetValue().ToDouble( &_kernel->divider );
	EndModal( wxID_OK );
}

void ConvolutionMatrixDlg::SetKernel( KERNEL_T* kernel )
{
	_kernel = kernel;
}
