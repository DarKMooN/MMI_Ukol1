#ifndef BITMAPI_H
#define BITMAPI_H

#include "defines.h"
#include "steganography.h"
#include "convolutionmatrixdlg.h"
#include <fstream>
#include <iostream>
#include <vector>
#include <memory>
#include <string>
#include <map>

using namespace std;

typedef struct {
	uint32_t dataIndex;
	uint32_t line;
	uint32_t validBytes;
	uint16_t bpp;
	uint16_t colorPlanes;
} BITMAP_INNER_STATE_STRUCT_T;

typedef struct {
	uint32_t width;
	uint32_t height;
} BITMAP_DIMENSIONS_T;

class BitmapI
{
public:
	BitmapI();
	virtual ~BitmapI();
	bool LoadImage( wxString inputPath );
	bool LoadImage( bool refresh = false);
	
	void Inverse();
	void Grayscale();
	void GrayscaleEmpire();	
	void Treshold();
	void Sepia();
	void Colorize( wxColour color );
	void MonoRed();
	void MonoGreen();
	void MonoBlue();
	
	void Mirror( bool horizontally = false );
	void Rotate( bool clockwise = false );
	void Scale( uint8_t percent );
	
	void Blur();
	void Focus();
	void EdgeDetect();
	void EdgeEnhacement();
	void Convolution();
	void Relief();
	void ColorRelief();
	void InverseLaplace();
	void Laplace();
	void Prewit();
	void Sobel();
	
	
	void DrawBitmap( wxClientDC *dc );
	int GetHeight();
	int GetWidth();
	BITMAP_DIMENSIONS_T GetDimensions();
	BITMAP_INNER_STATE_STRUCT_T GetState();
	string GetSteganography();
	bool SetSteganography( string input );
	

	static file_types_t GetFileType( wxString path );
	virtual vector<unsigned char> * GetBitmapData() = 0;

	virtual void GetImageData() = 0;
	virtual bool SaveBitmap( wxString path ) = 0;
	virtual bool SaveBitmap( wxString path , vector<unsigned char> * data, BITMAP_INNER_STATE_STRUCT_T state, BITMAP_DIMENSIONS_T dimens ) = 0;
	virtual bool SaveBitmap() = 0;
	virtual void GetProperties( wxPropertyGrid *properties ) = 0;
	virtual bool CanSteganography() = 0;
	virtual void ConvertSteganography() = 0;

protected:
	vector<unsigned char> file;
	vector<unsigned char> bitmapValidData;
	vector<wxColour> palete;
	wxString path;
	wxString fileType;
	BITMAP_INNER_STATE_STRUCT_T stateStruct;
	BITMAP_DIMENSIONS_T dimensions;
	BITMAP_DIMENSIONS_T originalDimensions;

	void BigEndian( uint16_t source, char ** dest );
	void BigEndian( uint32_t source, char ** dest );
	uint32_t GeneratePalet(map<uint32_t, uint16_t> *paleta);
	void ConvolutionMatrix( double matrix[3][3], double divider = 1, uint32_t offset = 0 );
	
};

#endif // BITMAPI_H
