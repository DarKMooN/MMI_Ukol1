#include "bitmapi.h"

BitmapI::BitmapI()
{
}

bool BitmapI::LoadImage( wxString inputPath )
{
	ifstream inputFile( inputPath, ios::in | ios::binary | ios::ate );
	if( inputFile.is_open() ) {
		path = inputPath;
		// načtení souboru
		streampos pos = inputFile.tellg();
		vector<char> tempVector( pos );

		inputFile.seekg( 0, ios::beg );
		inputFile.read( &tempVector[0], pos );
		inputFile.close();
		// převod na unsigned char z char
		file.clear();
		file.assign( tempVector.begin(), tempVector.end() );
		GetImageData(); // čtení obrazových dat
		return true;
	}
	wxMessageBox( wxT( "Soubor se nepodařilo otevřít" ) );
	return false;
}

bool BitmapI::LoadImage( bool refresh )
{
	if( path.IsEmpty() )
		return false;
	if( !refresh )
		return LoadImage( path );
	GetImageData();
	return true;
}

BitmapI::~BitmapI()
{
}

file_types_t BitmapI::GetFileType( wxString path )
{
	ifstream inputFile( path, ios::in | ios::binary );
	if( inputFile.is_open() ) {
		char tempArr[2];
		inputFile.read( tempArr, 2 );
		inputFile.close();

		uint16_t typeNum = MAKE16( tempArr, 0 );
		if( typeNum == BMP_TYPE )
			return bmp;
		else if( tempArr[0] == PCX_TYPE )
			return pcx;
		return unknown;
	}
	return unknown;
}

void BitmapI::DrawBitmap( wxClientDC* dc )
{
	if( bitmapValidData.empty() )
		return;
	unsigned char * data = ( unsigned char * )malloc( dimensions.width * dimensions.height * 3 * sizeof( unsigned char ) );
	memcpy( data, &bitmapValidData[0], stateStruct.dataIndex );
	wxImage image( dimensions.width, dimensions.height, data );
	wxBitmap bitmap( image );
	wxSize size = dc->GetSize();
	dc->Clear();

	wxSize drawSize( size.GetX() / 2 - dimensions.width / 2, size.GetY() / 2 - dimensions.height / 2 );

	if( ( uint32_t )size.GetWidth() < dimensions.width ) drawSize.SetWidth( 0 );
	if( ( uint32_t )size.GetHeight() < dimensions.height ) drawSize.SetHeight( 0 );

	dc->DrawBitmap( bitmap, drawSize.GetWidth(), drawSize.GetHeight() );
}

void BitmapI::BigEndian( uint16_t source, char ** dest )
{
	*( ( *dest )++ ) = ( char )( source & 0xff );
	*( ( *dest )++ ) = ( char )( ( source >> 8 ) & 0xff );
}

void BitmapI::BigEndian( uint32_t source, char ** dest )
{
	*( ( *dest )++ ) = ( char )( source & 0xff );
	*( ( *dest )++ ) = ( char )( ( source >> 8 ) & 0xff );
	*( ( *dest )++ ) = ( char )( ( source >> 16 ) & 0xff );
	*( ( *dest )++ ) = ( char )( ( source >> 24 ) & 0xff );
}

uint32_t BitmapI::GeneratePalet( map<uint32_t, uint16_t>* paleta )
{
	uint16_t colorCounter = 0;
	uint32_t color;

	// průchod všemi použitými barvami
	for( uint32_t i = 0; i < bitmapValidData.size(); i += 3 ) {
		color = ( bitmapValidData[i] << 16 ) | ( bitmapValidData[i+1] << 8 ) | bitmapValidData[i+2];
		if( paleta->find( color ) == paleta->end() ) {
			paleta->insert( pair<uint32_t, uint16_t>( color, colorCounter++ ) );
		}
	}
	// doplnění palety barev
	for( uint16_t i = colorCounter; i < pow( 2, stateStruct.bpp ); i++ )
		paleta->insert( pair<uint32_t, uint16_t>( 0x1ffffff + i, colorCounter++ ) );
	return colorCounter;
}

int BitmapI::GetHeight()
{
	return dimensions.height;
}

int BitmapI::GetWidth()
{
	return dimensions.width;
}

string BitmapI::GetSteganography()
{
	Steganography steg;
	return steg.GetSteganography( &bitmapValidData );
}

bool BitmapI::SetSteganography( string input )
{
	Steganography steg;
	return steg.SetSteganography( input, &bitmapValidData );
}

// efects
void BitmapI::Inverse()
{
	if( bitmapValidData.empty() )
		return;
	for( unsigned char &i : bitmapValidData ) {
		i = 0xff - i;
	}
}

void BitmapI::Grayscale()
{
	if( bitmapValidData.empty() )
		return;
	for( uint32_t i = 0; i < bitmapValidData.size(); i += 3 ) {
		unsigned char color = ( unsigned char )( ( ( bitmapValidData[i] + bitmapValidData[i+1] + bitmapValidData[i+2] ) / 3 ) & 0xff );
		bitmapValidData[i] = color;
		bitmapValidData[i+1] = color;
		bitmapValidData[i+2] = color;
	}
}

void BitmapI::GrayscaleEmpire()
{
	if( bitmapValidData.empty() )
		return;
	for( uint32_t i = 0; i < bitmapValidData.size(); i += 3 ) {
		unsigned char color = ( unsigned char )( ( ( 299 * bitmapValidData[i] + 587 * bitmapValidData[i + 1] + 114 * bitmapValidData[i + 2] ) / 1000 ) & 0xff );
		bitmapValidData[i] = color;
		bitmapValidData[i+1] = color;
		bitmapValidData[i+2] = color;
	}
}

void BitmapI::Treshold()
{
	if( bitmapValidData.empty() )
		return;
	uint8_t tresh = 125;
	for( uint32_t i = 0; i < bitmapValidData.size(); i += 3 ) {
		unsigned char color = ( ( ( bitmapValidData[i] + bitmapValidData[i+1] + bitmapValidData[i+2] ) / 3 ) & 0xff ) < tresh ? 0 : 0xff;
		bitmapValidData[i] = color;
		bitmapValidData[i+1] = color;
		bitmapValidData[i+2] = color;
	}
}

void BitmapI::Sepia()
{
	if( bitmapValidData.empty() )
		return;
	uint16_t r, g, b;
	for( uint32_t i = 0; i < bitmapValidData.size(); i += 3 ) {
		r = ( 393 * bitmapValidData[i] + 769 * bitmapValidData[i+1] + 189 * bitmapValidData[i+2] ) / 1000;
		r = r > 255 ? 255 : r;
		g = ( 349 * bitmapValidData[i] + 686 * bitmapValidData[i+1] + 168 * bitmapValidData[i+2] ) / 1000;
		g = g > 255 ? 255 : g;
		b = ( 272 * bitmapValidData[i] + 534 * bitmapValidData[i+1] + 131 * bitmapValidData[i+2] ) / 1000;
		b = b > 255 ? 255 : b;
		bitmapValidData[i] = ( uint8_t )r & 0xff;
		bitmapValidData[i+1] = ( uint8_t )g & 0xff;
		bitmapValidData[i+2] = ( uint8_t )b & 0xff;
	}
}

void BitmapI::Colorize( wxColour color )
{
	if( bitmapValidData.empty() )
		return;
	uint8_t r = color.Red(), g = color.Green(), b = color.Blue();
	int s;
	for( uint32_t i = 0; i < bitmapValidData.size(); i += 3 ) {
		s = ( 299 * bitmapValidData[i] + 587 * bitmapValidData[i+1] + 114 * bitmapValidData[i+2] )/1000;
		bitmapValidData[i] = ( s * r ) / 255;
		bitmapValidData[i+1] = ( s * g ) / 255;
		bitmapValidData[i+2] = ( s * b ) / 255;
	}
}

void BitmapI::MonoBlue()
{
	if( bitmapValidData.empty() )
		return;
	for( uint32_t i = 0; i < bitmapValidData.size(); i += 3 ) {
		bitmapValidData[i] = 0;
		bitmapValidData[i+1] = 0;
	}
}

void BitmapI::MonoGreen()
{
	if( bitmapValidData.empty() )
		return;
	for( uint32_t i = 0; i < bitmapValidData.size(); i += 3 ) {
		bitmapValidData[i] = 0;
		bitmapValidData[i+2] = 0;
	}
}

void BitmapI::MonoRed()
{
	if( bitmapValidData.empty() )
		return;
	for( uint32_t i = 0; i < bitmapValidData.size(); i += 3 ) {
		bitmapValidData[i+1] = 0;
		bitmapValidData[i+2] = 0;
	}
}

void BitmapI::Mirror( bool horizontally )
{
	vector<unsigned char> tempVector;
	tempVector.reserve( bitmapValidData.size() );
	tempVector.resize( bitmapValidData.size() );
	unsigned char *data = tempVector.data();
	const unsigned char *source_data = bitmapValidData.data();
	unsigned char *target_data;

	if ( horizontally ) {
		for ( long j = 0; j < dimensions.height; j++ ) {
			data += dimensions.width * 3;
			target_data = data - 3;
			for ( long i = 0; i < dimensions.width; i++ ) {
				memcpy( target_data, source_data, 3 );
				source_data += 3;
				target_data -= 3;
			}
		}
	} else {
		for ( long i = 0; i < dimensions.height; i++ ) {
			target_data = data + 3 * dimensions.width*( dimensions.height - 1 - i );
			memcpy( target_data, source_data, ( size_t )3 * dimensions.width );
			source_data += 3 * dimensions.width;
		}
	}
	copy( tempVector.begin(), tempVector.end(), bitmapValidData.begin() );
}

void BitmapI::Rotate( bool clockwise )
{
	vector<unsigned char> tempVector;
	tempVector.reserve( bitmapValidData.size() );
	tempVector.resize( bitmapValidData.size() );
	unsigned char *data = tempVector.data();
	unsigned char *target_data;

	for ( long ii = 0; ii < dimensions.width; ) {
		long next_ii = wxMin( ii + 21, dimensions.width );

		for ( long j = 0; j < dimensions.height; j++ ) {
			const unsigned char *source_data = bitmapValidData.data() + ( j * dimensions.width + ii ) * 3;

			for ( long i = ii; i < next_ii; i++ ) {
				if ( clockwise ) {
					target_data = data + ( ( i + 1 ) * dimensions.height - j - 1 ) * 3;
				} else {
					target_data = data + ( dimensions.height * ( dimensions.width - 1 - i ) + j ) * 3;
				}
				memcpy( target_data, source_data, 3 );
				source_data += 3;
			}
		}

		ii = next_ii;
	}
	std::copy( tempVector.begin(), tempVector.end(), bitmapValidData.begin() );
	uint16_t w = dimensions.width;
	dimensions.width = dimensions.height;
	dimensions.height = w;
}

void BitmapI::ConvolutionMatrix( double matrix[3][3], double divider, uint32_t offset )
{
	ConvertSteganography();
	vector<unsigned char> tempVector;
	tempVector.reserve( bitmapValidData.size() );
	tempVector.resize( bitmapValidData.size() );
	for( uint8_t i = 0; i < 3; i++ )
		for( uint8_t j = 0; j < 3; j++ )
			matrix[i][j] = matrix[i][j] / divider;
	for( uint32_t y = 0; y < dimensions.height; y++ ) {
		for( uint32_t x = 0; x < dimensions.width; x++ ) {
			double rtotal = 0, btotal = 0, gtotal = 0;
			for( int8_t i = -1; i < 2; i++ ) { //posun sloupce
				for( int8_t j = -1; j < 2; j++ ) { //posun řádek
					if( x + i < 0 || y + j < 0 || x + i >= dimensions.width || y + j >= dimensions.height ) {
						rtotal += 0;
						gtotal += 0;
						btotal += 0;
					} else {
						uint32_t index = ( ( ( y + j ) * dimensions.width + x ) + i ) * 3;
						rtotal += bitmapValidData[index] * matrix[i + 1][j + 1];
						gtotal += bitmapValidData[index + 1] * matrix[i + 1][j + 1];
						btotal += bitmapValidData[index + 2] * matrix[i + 1][j + 1];
					}
				}
			}
			rtotal = rtotal > 255 ? 255 : ( rtotal < 0 ? 0 : rtotal );
			gtotal = gtotal > 255 ? 255 : ( gtotal < 0 ? 0 : gtotal );
			btotal = btotal > 255 ? 255 : ( btotal < 0 ? 0 : btotal );
			tempVector[( y * dimensions.width + x ) * 3] = rtotal + offset;
			tempVector[( y * dimensions.width + x ) * 3 + 1] = gtotal + offset;
			tempVector[( y * dimensions.width + x ) * 3 + 2] = btotal + offset;
		}
	}
	copy( tempVector.begin(), tempVector.end(), bitmapValidData.begin() );
}

void BitmapI::Blur()
{
	double convolution[3][3] = {
		1/9.0, 1/9.0, 1/9.0,
		1/9.0, 1/9.0, 1/9.0,
		1/9.0, 1/9.0, 1/9.0
	};
	ConvolutionMatrix( convolution );
}

void BitmapI::Convolution()
{
	KERNEL_T kernel;
	ConvolutionMatrixDlg *dlg = new ConvolutionMatrixDlg( wxTheApp->GetTopWindow() );
	dlg->SetKernel( &kernel );
	if( dlg->ShowModal() == wxID_OK ) {
		double convolution[3][3] = {
			kernel.kernel[0], kernel.kernel[1], kernel.kernel[2],
			kernel.kernel[3], kernel.kernel[4], kernel.kernel[5],
			kernel.kernel[6], kernel.kernel[7], kernel.kernel[8]
		};
		ConvolutionMatrix( convolution, kernel.divider );
	}
	delete dlg;
}

void BitmapI::EdgeDetect()
{
	double convolution[3][3] = {
		0, 1, 0,
		1, -4, 1,
		0, 1, 0
	};
	ConvolutionMatrix( convolution );
}

void BitmapI::EdgeEnhacement()
{
	double convolution[3][3] = {
		-1/8.0, -1/8.0, -1/8.0,
		-1/8.0, 2, -1/8.0,
		-1/8.0, -1/8.0, -1/8.0
	};
	ConvolutionMatrix( convolution );
}

void BitmapI::Focus()
{
	double convolution[3][3] = {
		0, -1, 0,
		-1, 5, 1,
		0, -1, 0
	};
	ConvolutionMatrix( convolution );
}

void BitmapI::Relief()
{
	double convolution[3][3] = {
		0, 0, 0,
		0, 1, 0,
		0, 0, -1
	};
	ConvolutionMatrix( convolution, 1, 128 );
}

void BitmapI::ColorRelief()
{
	double convolution[3][3] = {
		1, 1, -1,
		1, 1, -1,
		1, -1, -1
	};
	ConvolutionMatrix( convolution );
}

void BitmapI::InverseLaplace()
{
	double convolution[3][3] = {
		0, -1, 0,
		-1, 5, -1,
		0, -1, 0
	};
	ConvolutionMatrix( convolution );
}

void BitmapI::Laplace()
{
	double convolution[3][3] = {
		0, 1, 0,
		1, -3, 1,
		0, 1, 0
	};
	ConvolutionMatrix( convolution );
}

void BitmapI::Prewit()
{
	double convolution[3][3] = {
		1, 1, 1,
		0, 1, 0,
		-1, -1, -1
	};
	ConvolutionMatrix( convolution );
}

void BitmapI::Sobel()
{
	double convolution[3][3] = {
		1, 2, 1,
		0, 1, 0,
		-1, -2, -1
	};
	ConvolutionMatrix( convolution );
}

vector<unsigned char>* BitmapI::GetBitmapData()
{
	return &bitmapValidData;
}

BITMAP_DIMENSIONS_T BitmapI::GetDimensions()
{
	return dimensions;
}

BITMAP_INNER_STATE_STRUCT_T BitmapI::GetState()
{
	return stateStruct;
}

void BitmapI::Scale( uint8_t percent )
{
	vector<unsigned char> tempVector;
	uint32_t width = originalDimensions.width * ( percent / 100.0 );
	uint32_t height = originalDimensions.height * ( percent / 100.0 );
	if(width < 1 || height < 1)
		return;
	GetImageData();
	if ( originalDimensions.width == width && originalDimensions.height == height )
		return;

	tempVector.reserve( width * height * 3 );
	tempVector.resize( width * height * 3 );

	unsigned char *data = tempVector.data();
	const unsigned char *source_data = bitmapValidData.data();
	unsigned char *target_data = data;

	long old_height = dimensions.height,
	     old_width  = dimensions.width;
	long x_delta = ( old_width << 16 ) / width;
	long y_delta = ( old_height << 16 ) / height;

	unsigned char* dest_pixel = target_data;

	long y = 0;
	for ( long j = 0; j < height; j++ ) {
		const unsigned char* src_line = &source_data[( y >> 16 ) * old_width * 3];

		long x = 0;
		for ( long i = 0; i < width; i++ ) {
			const unsigned char* src_pixel = &src_line[( x >> 16 ) * 3];
			dest_pixel[0] = src_pixel[0];
			dest_pixel[1] = src_pixel[1];
			dest_pixel[2] = src_pixel[2];
			dest_pixel += 3;
			x += x_delta;
		}
		y += y_delta;
	}
	bitmapValidData.clear();
	bitmapValidData.reserve( tempVector.size() );
	bitmapValidData.resize( tempVector.size() );

	copy( tempVector.begin(), tempVector.end(), bitmapValidData.begin() );
	dimensions.width = width;
	dimensions.height = height;
	stateStruct.dataIndex = bitmapValidData.size();
}
