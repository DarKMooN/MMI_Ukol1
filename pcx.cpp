#include "pcx.h"

PCX::PCX()
{
}

PCX::~PCX()
{
}

void PCX::GetImageData()
{
	//http://www.fileformat.info/format/pcx/spec/a10e75307b3a4cc49c3bbe6db4c41fa2/view.htm
	header = {
		.password = PCX_TYPE,
		.version = file[1],
		.coding = file[2],
		.Xmin = MAKE16( file, 4 ),
		.Ymin = MAKE16( file, 6 ),
		.Xmax = MAKE16( file, 8 ),
		.Ymax = MAKE16( file, 10 ),
		.HDpi = MAKE16( file, 12 ),
		.VDpi = MAKE16( file, 14 ),
		.BPL = MAKE16( file, 66 ),
		.paleteInfo = MAKE16( file, 68 )
	};

	palete.clear();


	dimensions.height = header.Ymax - header.Ymin + 1;
	dimensions.width = header.Xmax - header.Xmin + 1;
	originalDimensions.height = dimensions.height;
	originalDimensions.width = dimensions.width;

	// kolik místa je třeba reservovat na dekodovaný řádek
	stateStruct.colorPlanes = file[65];
	stateStruct.line = stateStruct.colorPlanes * header.BPL;
	stateStruct.bpp = file[3];

	// vyžadováno kódování
	if( header.coding != 1 ) {
		wxMessageBox( wxT( "Obrázek nelze dekódovat" ) );
		return;
	}

	stateStruct.dataIndex = 0;

	// alokace místa na data bitmapy
	unsigned char * data = ( unsigned char * ) malloc( dimensions.width * dimensions.height * 3 * sizeof( unsigned char ) );
	unsigned char * decodeData = ( unsigned char * ) malloc( MAX_DECODE_SIZE * sizeof( unsigned char ) );
	uint32_t index = 0x80;
	uint32_t actHeight = 0;

	int mask, shift, bitStart;
	switch( stateStruct.bpp ) {
	case 1:
		bitStart = 7;
		stateStruct.validBytes = ceil( dimensions.width / ( double )8 );
		mask = 0x1;
		shift = 1;
		break;
	case 4:
		stateStruct.validBytes = ceil( dimensions.width / ( double )2 );
		mask = 0xf;
		shift = 4;
		bitStart = 4;
		break;
	case 8:
		shift = 8;
		stateStruct.validBytes = dimensions.width;
		bitStart = 0;
		mask = 0xff;
		break;
	default:
		bitStart = -1;
		stateStruct.validBytes = mask = shift = 0;
		break;
	}

	// 1b, 4b, 8b - čtu paletu
	if( stateStruct.bpp == 1 || stateStruct.bpp == 4 || ( stateStruct.bpp == 8 && stateStruct.colorPlanes == 1 ) ) {
		unsigned char * tempData = ( unsigned char * ) malloc( stateStruct.line * dimensions.height * sizeof( unsigned char ) );
		unsigned char * tempDataPtr = tempData; // zapamatování si alokovaného místa

		while( index < file.size() && actHeight < dimensions.height ) {
			uint8_t read = 0;
			do {
				uint8_t kolik = DecodeData( decodeData, &file[index] );
				memcpy( tempData, decodeData, kolik );
				read += kolik;
				tempData += kolik;
				index += file[index] > 0xc0 ? 2 : 1;
			} while( read < header.BPL );
			tempData -= stateStruct.line - stateStruct.validBytes; // oříznutí nepotřebných bytů
			actHeight++;
		}
		while( file[index++] != 0xc );
		// 8b - paleta je za daty
		if( stateStruct.bpp == 8 ) {
			GetPalete( &file[index] );
		} else { // paleta v hlavičce
			GetPalete( &file[0x10] );
		}

		tempData = tempDataPtr;
		for( uint32_t i = 0; i < dimensions.height; i++ ) {
			uint16_t actWidth = 0;
			for( uint32_t j = 0; j < stateStruct.validBytes; j++ ) {
				// vymaskování bitů podle bpp (pro bpp = 1 musí být 8 průchodů, pro bpp = 4 musí být 2 průchody, pro bpp = 8 musí být 1 průchod. bpp = 24 má zvlášní handler)
				// actWidth ukončí procházení poté, co se dosáhne šířky
				for( int bits = bitStart ; bits >= 0 && actWidth < dimensions.width; bits -= shift, actWidth++ ) {
					unsigned char index = ( *tempData >> bits ) & mask;
					data[stateStruct.dataIndex++] = palete[ index ].Blue();
					data[stateStruct.dataIndex++] = palete[ index ].Green();
					data[stateStruct.dataIndex++] = palete[ index ].Red();
					//cout << ( index == 0 ? "*" : "x" );
				}
				tempData++;
			}
			//cout << endl;
		}
		free( tempDataPtr );
	}

	// 24b - 3 roviny barev
	else if( stateStruct.colorPlanes == 3 ) {
		stateStruct.validBytes = dimensions.width;
		// aklokace místa pro planes
		unsigned char * plane = ( unsigned char * ) malloc( 3 * stateStruct.line * sizeof( unsigned char ) );
		while( index < file.size() && actHeight < dimensions.height ) {
			uint16_t read = 0;
			while( index < file.size() && read < header.BPL * 3 ) {
				uint8_t kolik = DecodeData( decodeData, &file[index] );
				// zkopíruje data na správnou barevnou složku a na správnou pozici
				memcpy( &plane[read], decodeData, kolik );
				read += kolik;
				index += file[index] > 0xc0 ? 2 : 1;
			}
			for( uint32_t i = 0; i < dimensions.width; i++ ) {
				data[stateStruct.dataIndex++] = plane[i];
				data[stateStruct.dataIndex++] = plane[i+header.BPL];
				data[stateStruct.dataIndex++] = plane[i+2*header.BPL];
			}
			actHeight++;
		}
		// úklid
		free( plane );
	} else {
		wxMessageBox( wxT( "Obrázek nelze dekódovat" ) );
	}

	bitmapValidData.clear();
	bitmapValidData.reserve( stateStruct.dataIndex );
	bitmapValidData.resize( stateStruct.dataIndex );
	memcpy( &bitmapValidData[0], data, stateStruct.dataIndex );
	// úklid
	free( decodeData );
	free( data );
}

void PCX::GetProperties( wxPropertyGrid* properties )
{
	properties->Clear();
	properties->Append( new wxIntProperty( wxT( "Výška" ), "Height", dimensions.height ) );
	properties->Append( new wxIntProperty( wxT( "Šířka" ), "Width", dimensions.width ) );
	properties->Append( new wxIntProperty( wxT( "Bpp" ), "Bpp", stateStruct.bpp ) );
	properties->Append( new wxIntProperty( wxT( "Obrazových rovin" ), "Planes", stateStruct.colorPlanes ) );
	properties->Append( new wxIntProperty( wxT( "BPL" ), "BPL", header.BPL ) );
	properties->Append( new wxIntProperty( wxT( "VB" ), "VB", stateStruct.validBytes ) );
}

bool PCX::SaveBitmap( wxString path )
{
	ofstream ofile( path, ios::out | ios::trunc | ios::binary );
	if( ofile.is_open() ) {

		map<uint32_t, uint16_t> paleta;
		//paleta.clear();
		uint32_t paletSize = GeneratePalet( &paleta );

		uint8_t startBit, divider;
		switch( stateStruct.bpp ) {
		case 1:
			startBit = 7;
			divider = 8;
			break;
		case 4:
			startBit = 4;
			divider = 2;
			break;
		case 8:
			startBit = 0;
			divider = 1;
			break;
		case 24:
			break;
		}
		stateStruct.line = ceil( ( dimensions.width * stateStruct.bpp / ( double ) 16 ) ) * 2;
		stateStruct.validBytes = ceil( dimensions.width /  ( double )divider );

		char * tempVector = ( char * )malloc( PCX_HEADER_SIZE + paletSize + stateStruct.line * dimensions.height * 4 * sizeof( char ) );
		memset( tempVector, 0, PCX_HEADER_SIZE + paletSize + stateStruct.line * dimensions.height * 4 * sizeof( char ) );

		char * tempVectorDataPtr = tempVector;

		// vytvoření hlavičky souboru
		*tempVectorDataPtr++ = PCX_TYPE; 	// identifikátor PCX
		*tempVectorDataPtr++ = 5; 			// verze PCX = 5
		*tempVectorDataPtr++ = 1; 			// kódování RLE = 1
		*tempVectorDataPtr++ = stateStruct.bpp;	// bpp
		BigEndian( ( uint16_t )0, &tempVectorDataPtr ); // Xmin
		BigEndian( ( uint16_t )0, &tempVectorDataPtr ); // Ymin
		BigEndian( ( uint16_t )( dimensions.width - 1 ), &tempVectorDataPtr ); // Xmax
		BigEndian( ( uint16_t )( dimensions.height - 1 ), &tempVectorDataPtr ); // Ymax
		BigEndian( ( uint16_t )0, &tempVectorDataPtr ); // HDpi
		BigEndian( ( uint16_t )0, &tempVectorDataPtr ); // VDpi
		tempVectorDataPtr += 48;			// rezervace místa pro paletu
		*tempVectorDataPtr++ = 0; 			// reserved = 0
		*tempVectorDataPtr++ = stateStruct.colorPlanes; 			// počet rovin
		BigEndian( ( uint16_t )stateStruct.line, &tempVectorDataPtr ); // BPL
		BigEndian( ( uint16_t )1, &tempVectorDataPtr ); // bezpředmětné info

		memset( tempVectorDataPtr, 0, 58 ); // doplnění hlavičky nulamy
		char str[] = "Tomas Jurena IT 2015";
		memcpy( tempVectorDataPtr, &str, 21 );

		tempVectorDataPtr = tempVector + PCX_HEADER_SIZE;

		unsigned char * data = ( unsigned char * ) malloc( 3 * dimensions.width * dimensions.height * sizeof( unsigned char ) );
		unsigned char * dataPtr = data;
		uint32_t totalData = 0;

		int8_t bits = startBit;
		uint32_t actWidth = 0;
		*data = 0;
		for( uint32_t i = 0; i < stateStruct.dataIndex; i += 3, actWidth++ ) {
			// průchod pro 24b obrázky
			if( stateStruct.bpp == 8 && stateStruct.colorPlanes == 3 ) {
				if( actWidth == dimensions.width ) {
					actWidth = 0;
					totalData += 2 * stateStruct.validBytes;
				}
				data[totalData] = bitmapValidData[i];
				data[totalData + stateStruct.validBytes] = bitmapValidData[i + 1];
				data[totalData + stateStruct.validBytes * 2] = bitmapValidData[i + 2];
				totalData++;
			} else {
				if( actWidth == dimensions.width ) {
					actWidth = 0;
					if( stateStruct.validBytes * divider > dimensions.width )
						*++data = 0;
					bits = startBit;
				}
				// vytvoření barvy
				uint32_t color = ( bitmapValidData[i] << 16 ) | ( bitmapValidData[i+1] << 8 ) | bitmapValidData[i+2];
				// vyhledání indexu barvy v paletě
				uint16_t colorIndex = paleta[color];
				*data |= colorIndex << bits;
				if( bits == 0 || stateStruct.bpp == 8 ) {
					*++data = 0;
					bits = startBit;
				} else
					bits -= stateStruct.bpp;
			}

		}

		uint32_t encodedData = EncodeData( tempVectorDataPtr, dataPtr );

		if( stateStruct.bpp == 1 || stateStruct.bpp == 4 || ( stateStruct.bpp == 8 && stateStruct.colorPlanes != 3 ) ) {
			map<uint16_t, uint32_t> tempPaletMap;
			// vypsání palety barev
			for( pair<uint32_t, uint16_t> i : paleta ) {
				tempPaletMap.insert( pair<uint16_t, uint32_t>( i.second, i.first ) );
			}
			// vypsání palety ve správném pořadí
			if( stateStruct.bpp == 1 || stateStruct.bpp == 4 )
				tempVectorDataPtr = tempVector + 16;
			else {
				tempVectorDataPtr += encodedData;
				*tempVectorDataPtr++ = 0x0c;
				paletSize++;
			}
			for( pair<uint16_t, uint32_t> i : tempPaletMap ) {
				*tempVectorDataPtr++ = ( i.second >> 16 ) & 0xff;
				*tempVectorDataPtr++ = ( i.second >> 8 ) & 0xff;
				*tempVectorDataPtr++ = ( i.second & 0xff );
			}
		}

		ofile.write( tempVector, PCX_HEADER_SIZE + encodedData + paletSize );
		ofile.close();
		free( dataPtr );
		free( tempVector );
		return true;
	}
	return false;
}

uint16_t PCX::GetPalete( unsigned char * from )
{
	uint16_t paletSize = pow( 2, stateStruct.bpp );
	palete.clear();
	palete.reserve( paletSize );
	for( uint16_t i = 0; i < paletSize && &from[i*3+2] <= &file.back(); i++ ) {
		palete.push_back( wxColour( from[i*3] << 16 | from[i*3+1] << 8 | from[i*3+2] ) );
	}
	return paletSize;
}

uint32_t PCX::GeneratePalet( map<uint32_t, uint16_t>* paleta )
{
	// 25b obrázek nemá paletu
	if( stateStruct.bpp == 8 && stateStruct.colorPlanes == 3 )
		return 0;
	return BitmapI::GeneratePalet( paleta ) * ( stateStruct.bpp == 1 || stateStruct.bpp == 4 || ( stateStruct.bpp == 8 && stateStruct.colorPlanes == 3 ) ? 0 : 3 );
}

uint8_t PCX::DecodeData( unsigned char* dest, unsigned char* source )
{
	// pokud hodnota na adrese není kodovaná, tak se pouze vrátí
	if( *source < 0xc0 ) {
		*dest = *source;
		return 1;
	}
	uint8_t repeat = *source & 0x3f; // výpočet kolikrát byt zopakovat
	memset( dest, *( source + 1 ), repeat ); // zopakování bytu
	return repeat; // vrátí počet načtených bytů
}

uint32_t PCX::EncodeData( char* dest, unsigned char* source )
{
	char * startAdr = dest;
	for( uint16_t actHeight = 0; actHeight < dimensions.height * stateStruct.colorPlanes; actHeight++ ) {
		uint8_t count;
		uint16_t i;
		for( i = 0; i < stateStruct.validBytes;  ) {
			count = 0xc0;
			unsigned char act = *source;
			while( *source++ == act && i++ < stateStruct.validBytes && ++count < 0xff );
			if( count > 0xc1 ) {
				*dest++ = count;
				*dest++ = act;
			} else {
				if( act < 0xc0 )
					*dest++ = act;
				else {
					*dest++ = count;
					*dest++ = act;
				}
			}
			if( count != 0xff )
				source--; // posun zpět na platný prvek
		}
		if( stateStruct.validBytes != stateStruct.line ) {
			dest++;
		}
	}
	return dest - startAdr;
}
bool PCX::SaveBitmap()
{
	return SaveBitmap( path );
}

bool PCX::CanSteganography()
{
	return stateStruct.bpp == 8 && stateStruct.colorPlanes == 3;
}

void PCX::ConvertSteganography()
{
	stateStruct.bpp = 8;
	stateStruct.colorPlanes = 3;
}

bool PCX::SaveBitmap( wxString path , vector<unsigned char> * data, BITMAP_INNER_STATE_STRUCT_T state, BITMAP_DIMENSIONS_T dimens )
{
	bitmapValidData = *data;
	stateStruct = state;
	dimensions = dimens;
	return SaveBitmap( path );
}
vector<unsigned char>* PCX::GetBitmapData()
{
	stateStruct.bpp *= stateStruct.colorPlanes;
	return &bitmapValidData;
}
