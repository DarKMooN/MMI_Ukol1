#ifndef BMP_H
#define BMP_H

#include "defines.h"
#include "bitmapi.h" // Base class: BitmapI
#include <map>
#include <cmath>

//http://www.fastgraph.com/help/bmp_header_format.html
typedef struct {
	uint16_t type;
	uint32_t size;
	uint16_t reserved1;
	uint16_t reserved2;
	uint32_t startImageData;
} BMP_FILE_HEADER_T;

typedef struct {
	uint32_t size;
	uint32_t width;
	uint32_t height;
	uint16_t planes;
	uint32_t compression;
	uint32_t imageSize;
	uint32_t hRes; // horizontal resolution per meter
	uint32_t vRes; // vertical resolution per meter
	uint32_t colorsUsed; // Number of colors used
	uint32_t colorsImportant; // Number of important colors
} BMP_INFO_HEADER_T;

class BMP : public BitmapI
{
public:
	virtual
	vector<unsigned char>* GetBitmapData();
	virtual
	bool SaveBitmap( wxString path , vector<unsigned char> * data, BITMAP_INNER_STATE_STRUCT_T state, BITMAP_DIMENSIONS_T dimens );
	virtual
	bool CanSteganography();
	virtual
	void ConvertSteganography();
	virtual bool SaveBitmap();
	BMP();
	BMP( wxString filePath, bool loadFile = true );
	BMP( wxString filePath, wxClientDC *dc );
	BMP( wxString filePath, wxClientDC *dc, wxPropertyGrid *properties );
	virtual ~BMP();
	virtual void GetProperties( wxPropertyGrid *properties );
	virtual bool SaveBitmap( wxString path );

private:
	BMP_FILE_HEADER_T header;
	BMP_INFO_HEADER_T fileInfo;

	virtual void GetImageData();
	uint32_t GeneratePalet( map<uint32_t, uint16_t> *paleta );
};

#endif // BMP_H
