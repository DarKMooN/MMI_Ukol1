///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun  6 2014)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "gui.h"

///////////////////////////////////////////////////////////////////////////

MainFrameBase::MainFrameBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	m_menuBar = new wxMenuBar( 0 );
	m_menuFile = new wxMenu();
	wxMenuItem* menuFileOpen;
	menuFileOpen = new wxMenuItem( m_menuFile, wxID_ANY, wxString( wxT("&Otevřít") ) + wxT('\t') + wxT("CTRL+O"), wxEmptyString, wxITEM_NORMAL );
	m_menuFile->Append( menuFileOpen );
	
	m_menuFile->AppendSeparator();
	
	wxMenuItem* menuFileSave;
	menuFileSave = new wxMenuItem( m_menuFile, wxID_ANY, wxString( wxT("Uložit") ) + wxT('\t') + wxT("CTRL+S"), wxEmptyString, wxITEM_NORMAL );
	m_menuFile->Append( menuFileSave );
	
	wxMenuItem* menuFileSaveAs;
	menuFileSaveAs = new wxMenuItem( m_menuFile, wxID_ANY, wxString( wxT("Uložit jako") ) + wxT('\t') + wxT("CTRL+SHIFT+S"), wxEmptyString, wxITEM_NORMAL );
	m_menuFile->Append( menuFileSaveAs );
	
	m_menuFile->AppendSeparator();
	
	wxMenuItem* menuFileRefresh;
	menuFileRefresh = new wxMenuItem( m_menuFile, wxID_ANY, wxString( wxT("Obnovit") ) + wxT('\t') + wxT("CTRL+R"), wxEmptyString, wxITEM_NORMAL );
	m_menuFile->Append( menuFileRefresh );
	
	m_menuFile->AppendSeparator();
	
	wxMenuItem* menuFileExit;
	menuFileExit = new wxMenuItem( m_menuFile, wxID_EXIT, wxString( wxT("E&xit") ) + wxT('\t') + wxT("Alt+X"), wxEmptyString, wxITEM_NORMAL );
	m_menuFile->Append( menuFileExit );
	
	m_menuBar->Append( m_menuFile, wxT("Soubor") ); 
	
	m_menuView = new wxMenu();
	wxMenuItem* menuViewZoomIn;
	menuViewZoomIn = new wxMenuItem( m_menuView, wxID_ANY, wxString( wxT("Přiblížit") ) + wxT('\t') + wxT("+"), wxEmptyString, wxITEM_NORMAL );
	m_menuView->Append( menuViewZoomIn );
	
	wxMenuItem* menuViewZoomOut;
	menuViewZoomOut = new wxMenuItem( m_menuView, wxID_ANY, wxString( wxT("Oddálit") ) + wxT('\t') + wxT("-"), wxEmptyString, wxITEM_NORMAL );
	m_menuView->Append( menuViewZoomOut );
	
	m_menuBar->Append( m_menuView, wxT("Zobrazeni") ); 
	
	m_menuEdit = new wxMenu();
	m_subMenuColors = new wxMenu();
	wxMenuItem* m_subMenuColorsItem = new wxMenuItem( m_menuEdit, wxID_ANY, wxT("Barvy"), wxEmptyString, wxITEM_NORMAL, m_subMenuColors );
	wxMenuItem* m_submenuColorsInvert;
	m_submenuColorsInvert = new wxMenuItem( m_subMenuColors, wxID_ANY, wxString( wxT("Invertovat") ) + wxT('\t') + wxT("CTRL+I"), wxEmptyString, wxITEM_NORMAL );
	m_subMenuColors->Append( m_submenuColorsInvert );
	
	wxMenuItem* m_submenuColorsMonoGray;
	m_submenuColorsMonoGray = new wxMenuItem( m_subMenuColors, wxID_ANY, wxString( wxT("Monochromatická šedá") ) , wxEmptyString, wxITEM_NORMAL );
	m_subMenuColors->Append( m_submenuColorsMonoGray );
	
	wxMenuItem* m_submenuColorsMonoRed;
	m_submenuColorsMonoRed = new wxMenuItem( m_subMenuColors, wxID_ANY, wxString( wxT("Monochromatická červená") ) , wxEmptyString, wxITEM_NORMAL );
	m_subMenuColors->Append( m_submenuColorsMonoRed );
	
	wxMenuItem* m_submenuColorsMonoBlue;
	m_submenuColorsMonoBlue = new wxMenuItem( m_subMenuColors, wxID_ANY, wxString( wxT("Monochromatická modrá") ) , wxEmptyString, wxITEM_NORMAL );
	m_subMenuColors->Append( m_submenuColorsMonoBlue );
	
	wxMenuItem* m_submenuColorsMonoGreen;
	m_submenuColorsMonoGreen = new wxMenuItem( m_subMenuColors, wxID_ANY, wxString( wxT("Monochromatická zelená") ) , wxEmptyString, wxITEM_NORMAL );
	m_subMenuColors->Append( m_submenuColorsMonoGreen );
	
	wxMenuItem* m_submenuColorColorize;
	m_submenuColorColorize = new wxMenuItem( m_subMenuColors, wxID_ANY, wxString( wxT("Odbarvit") ) , wxEmptyString, wxITEM_NORMAL );
	m_subMenuColors->Append( m_submenuColorColorize );
	
	wxMenuItem* m_submenuColorTreshold;
	m_submenuColorTreshold = new wxMenuItem( m_subMenuColors, wxID_ANY, wxString( wxT("Práh") ) , wxEmptyString, wxITEM_NORMAL );
	m_subMenuColors->Append( m_submenuColorTreshold );
	
	wxMenuItem* m_submenuColorSepia;
	m_submenuColorSepia = new wxMenuItem( m_subMenuColors, wxID_ANY, wxString( wxT("Sépie") ) , wxEmptyString, wxITEM_NORMAL );
	m_subMenuColors->Append( m_submenuColorSepia );
	
	m_menuEdit->Append( m_subMenuColorsItem );
	
	m_subMenuTranform = new wxMenu();
	wxMenuItem* m_subMenuTranformItem = new wxMenuItem( m_menuEdit, wxID_ANY, wxT("Transformace"), wxEmptyString, wxITEM_NORMAL, m_subMenuTranform );
	wxMenuItem* m_submenuTransformRotateLeft;
	m_submenuTransformRotateLeft = new wxMenuItem( m_subMenuTranform, wxID_ANY, wxString( wxT("Rotovat doleva") ) , wxEmptyString, wxITEM_NORMAL );
	m_subMenuTranform->Append( m_submenuTransformRotateLeft );
	
	wxMenuItem* m_submenuTransformRotateRight;
	m_submenuTransformRotateRight = new wxMenuItem( m_subMenuTranform, wxID_ANY, wxString( wxT("Rotovat doprava") ) , wxEmptyString, wxITEM_NORMAL );
	m_subMenuTranform->Append( m_submenuTransformRotateRight );
	
	wxMenuItem* m_submenuTransformMirrorHorizontaly;
	m_submenuTransformMirrorHorizontaly = new wxMenuItem( m_subMenuTranform, wxID_ANY, wxString( wxT("Zrcadlit horizontálně") ) , wxEmptyString, wxITEM_NORMAL );
	m_subMenuTranform->Append( m_submenuTransformMirrorHorizontaly );
	
	wxMenuItem* m_submenuTransformMirrorVerticaly;
	m_submenuTransformMirrorVerticaly = new wxMenuItem( m_subMenuTranform, wxID_ANY, wxString( wxT("Zrcadlit vertikálně") ) , wxEmptyString, wxITEM_NORMAL );
	m_subMenuTranform->Append( m_submenuTransformMirrorVerticaly );
	
	m_menuEdit->Append( m_subMenuTranformItem );
	
	m_subMenuFilters = new wxMenu();
	wxMenuItem* m_subMenuFiltersItem = new wxMenuItem( m_menuEdit, wxID_ANY, wxT("Filtry"), wxEmptyString, wxITEM_NORMAL, m_subMenuFilters );
	wxMenuItem* m_subMenuFiltersFocus;
	m_subMenuFiltersFocus = new wxMenuItem( m_subMenuFilters, wxID_ANY, wxString( wxT("Zaostřit") ) , wxEmptyString, wxITEM_NORMAL );
	m_subMenuFilters->Append( m_subMenuFiltersFocus );
	
	wxMenuItem* m_subMenuFiltersBlur;
	m_subMenuFiltersBlur = new wxMenuItem( m_subMenuFilters, wxID_ANY, wxString( wxT("Rozostřit") ) , wxEmptyString, wxITEM_NORMAL );
	m_subMenuFilters->Append( m_subMenuFiltersBlur );
	
	wxMenuItem* m_subMenuFiltersEdgeEnhacement;
	m_subMenuFiltersEdgeEnhacement = new wxMenuItem( m_subMenuFilters, wxID_ANY, wxString( wxT("Zvýraznění hran") ) , wxEmptyString, wxITEM_NORMAL );
	m_subMenuFilters->Append( m_subMenuFiltersEdgeEnhacement );
	
	wxMenuItem* m_subMenuFiltersEdgeDetection;
	m_subMenuFiltersEdgeDetection = new wxMenuItem( m_subMenuFilters, wxID_ANY, wxString( wxT("Detekce hran") ) , wxEmptyString, wxITEM_NORMAL );
	m_subMenuFilters->Append( m_subMenuFiltersEdgeDetection );
	
	wxMenuItem* m_subMenuFiltersRelief;
	m_subMenuFiltersRelief = new wxMenuItem( m_subMenuFilters, wxID_ANY, wxString( wxT("Reliéf") ) , wxEmptyString, wxITEM_NORMAL );
	m_subMenuFilters->Append( m_subMenuFiltersRelief );
	
	wxMenuItem* m_subMenuFiltersColorRelief;
	m_subMenuFiltersColorRelief = new wxMenuItem( m_subMenuFilters, wxID_ANY, wxString( wxT("Barevný reliéf") ) , wxEmptyString, wxITEM_NORMAL );
	m_subMenuFilters->Append( m_subMenuFiltersColorRelief );
	
	wxMenuItem* m_subMenuFiltersLaplaceSharpening;
	m_subMenuFiltersLaplaceSharpening = new wxMenuItem( m_subMenuFilters, wxID_ANY, wxString( wxT("Zaostření pomocí Laplaceova operátoru") ) , wxEmptyString, wxITEM_NORMAL );
	m_subMenuFilters->Append( m_subMenuFiltersLaplaceSharpening );
	
	wxMenuItem* m_subMenuFiltersPrewitSharpening;
	m_subMenuFiltersPrewitSharpening = new wxMenuItem( m_subMenuFilters, wxID_ANY, wxString( wxT("Zaostření pomocí operátoru Prewittové") ) , wxEmptyString, wxITEM_NORMAL );
	m_subMenuFilters->Append( m_subMenuFiltersPrewitSharpening );
	
	wxMenuItem* m_subMenuFiltersSobelSharpening;
	m_subMenuFiltersSobelSharpening = new wxMenuItem( m_subMenuFilters, wxID_ANY, wxString( wxT("Zaostření pomocí Sobelova operátoru") ) , wxEmptyString, wxITEM_NORMAL );
	m_subMenuFilters->Append( m_subMenuFiltersSobelSharpening );
	
	wxMenuItem* m_subMenuFiltersInverseLaplaceSharpening;
	m_subMenuFiltersInverseLaplaceSharpening = new wxMenuItem( m_subMenuFilters, wxID_ANY, wxString( wxT("Zaostření pomocí invertovaného Laplaceova operátoru") ) , wxEmptyString, wxITEM_NORMAL );
	m_subMenuFilters->Append( m_subMenuFiltersInverseLaplaceSharpening );
	
	wxMenuItem* m_subMenuFiltersConvolutionMatrix;
	m_subMenuFiltersConvolutionMatrix = new wxMenuItem( m_subMenuFilters, wxID_ANY, wxString( wxT("Konvoluční matice") ) , wxEmptyString, wxITEM_NORMAL );
	m_subMenuFilters->Append( m_subMenuFiltersConvolutionMatrix );
	
	m_menuEdit->Append( m_subMenuFiltersItem );
	
	wxMenuItem* menuEditSteganography;
	menuEditSteganography = new wxMenuItem( m_menuEdit, wxID_ANY, wxString( wxT("Steganografie") ) + wxT('\t') + wxT("CTRL+ALT+S"), wxEmptyString, wxITEM_NORMAL );
	m_menuEdit->Append( menuEditSteganography );
	
	m_menuBar->Append( m_menuEdit, wxT("Úpravy") ); 
	
	m_menuHelp = new wxMenu();
	wxMenuItem* menuHelpAbout;
	menuHelpAbout = new wxMenuItem( m_menuHelp, wxID_ANY, wxString( wxT("O programu") ) + wxT('\t') + wxT("F1"), wxEmptyString, wxITEM_NORMAL );
	m_menuHelp->Append( menuHelpAbout );
	
	m_menuBar->Append( m_menuHelp, wxT("Nápověda") ); 
	
	this->SetMenuBar( m_menuBar );
	
	m_toolBar1 = this->CreateToolBar( wxTB_HORIZONTAL, wxID_ANY ); 
	m_toolOpen = m_toolBar1->AddTool( wxID_ANY, wxT("Otevřít"), wxArtProvider::GetBitmap( wxART_FILE_OPEN, wxART_TOOLBAR ), wxNullBitmap, wxITEM_NORMAL, wxT("Otevřít"), wxEmptyString, NULL ); 
	
	m_toolSave = m_toolBar1->AddTool( wxID_ANY, wxT("tool"), wxArtProvider::GetBitmap( wxART_FILE_SAVE, wxART_TOOLBAR ), wxNullBitmap, wxITEM_NORMAL, wxT("Uložit"), wxEmptyString, NULL ); 
	
	m_toolSaveAs = m_toolBar1->AddTool( wxID_ANY, wxT("tool"), wxArtProvider::GetBitmap( wxART_FILE_SAVE_AS, wxART_TOOLBAR ), wxNullBitmap, wxITEM_NORMAL, wxT("Uložit jako"), wxEmptyString, NULL ); 
	
	m_toolRefresh = m_toolBar1->AddTool( wxID_ANY, wxT("tool"), wxBitmap( wxT("icon/icon_refresh.png"), wxBITMAP_TYPE_ANY ), wxNullBitmap, wxITEM_NORMAL, wxT("Obnovit soubor"), wxEmptyString, NULL ); 
	
	m_toolBar1->AddSeparator(); 
	
	m_toolRotateL = m_toolBar1->AddTool( wxID_ANY, wxT("tool"), wxBitmap( wxT("icon/icon_rotate_left.png"), wxBITMAP_TYPE_ANY ), wxNullBitmap, wxITEM_NORMAL, wxT("Rotovat doleva"), wxEmptyString, NULL ); 
	
	m_toolRotateR = m_toolBar1->AddTool( wxID_ANY, wxT("tool"), wxBitmap( wxT("icon/icon_rotate_right.png"), wxBITMAP_TYPE_ANY ), wxNullBitmap, wxITEM_NORMAL, wxT("Rotovat doprava"), wxEmptyString, NULL ); 
	
	m_toolMirrorV = m_toolBar1->AddTool( wxID_ANY, wxT("tool"), wxBitmap( wxT("icon/icon_horizontal_flip.png"), wxBITMAP_TYPE_ANY ), wxNullBitmap, wxITEM_NORMAL, wxT("Zrcadlit horizontálně"), wxEmptyString, NULL ); 
	
	m_toolMirrorH = m_toolBar1->AddTool( wxID_ANY, wxT("tool"), wxBitmap( wxT("icon/icon_vertical_flip.png"), wxBITMAP_TYPE_ANY ), wxNullBitmap, wxITEM_NORMAL, wxT("Zrcadlit horizontálně"), wxEmptyString, NULL ); 
	
	m_toolBar1->AddSeparator(); 
	
	m_toolInverse = m_toolBar1->AddTool( wxID_ANY, wxT("tool"), wxBitmap( wxT("icon/icon_inverse.png"), wxBITMAP_TYPE_ANY ), wxNullBitmap, wxITEM_NORMAL, wxT("Invertovat barvy"), wxEmptyString, NULL ); 
	
	m_toolColorize = m_toolBar1->AddTool( wxID_ANY, wxT("tool"), wxBitmap( wxT("icon/icon_rgb_color.png"), wxBITMAP_TYPE_ANY ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString, NULL ); 
	
	m_toolBar1->AddSeparator(); 
	
	m_toolSteganography = m_toolBar1->AddTool( wxID_ANY, wxT("tool"), wxBitmap( wxT("icon/icon_steganography.png"), wxBITMAP_TYPE_ANY ), wxNullBitmap, wxITEM_NORMAL, wxT("Steganografie"), wxEmptyString, NULL ); 
	
	m_staticText1 = new wxStaticText( m_toolBar1, wxID_ANY, wxT("Zoom: "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1->Wrap( -1 );
	m_toolBar1->AddControl( m_staticText1 );
	m_scaleSlider = new wxSlider( m_toolBar1, wxID_ANY, 100, 10, 200, wxDefaultPosition, wxSize( 100,-1 ), wxSL_HORIZONTAL );
	m_toolBar1->AddControl( m_scaleSlider );
	m_zoomLVL = new wxStaticText( m_toolBar1, wxID_ANY, wxT("100%"), wxDefaultPosition, wxDefaultSize, 0 );
	m_zoomLVL->Wrap( -1 );
	m_toolBar1->AddControl( m_zoomLVL );
	m_toolBar1->Realize(); 
	
	wxBoxSizer* mainSizer;
	mainSizer = new wxBoxSizer( wxHORIZONTAL );
	
	bSizer2 = new wxBoxSizer( wxVERTICAL );
	
	canvas = new wxScrolledWindow( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHSCROLL|wxVSCROLL );
	canvas->SetScrollRate( 5, 5 );
	bSizer2->Add( canvas, 1, wxEXPAND, 5 );
	
	
	mainSizer->Add( bSizer2, 1, wxEXPAND, 5 );
	
	properties = new wxPropertyGrid(this, wxID_ANY, wxDefaultPosition, wxSize( 250,-1 ), wxPG_DEFAULT_STYLE);
	properties->Enable( false );
	
	mainSizer->Add( properties, 0, wxEXPAND, 5 );
	
	
	this->SetSizer( mainSizer );
	this->Layout();
	m_statusBar = this->CreateStatusBar( 2, wxST_SIZEGRIP, wxID_ANY );
	
	this->Centre( wxBOTH );
	
	// Connect Events
	this->Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( MainFrameBase::OnCloseFrame ) );
	this->Connect( menuFileOpen->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnOpenClick ) );
	this->Connect( menuFileSave->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnSaveClick ) );
	this->Connect( menuFileSaveAs->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnSaveAsClick ) );
	this->Connect( menuFileRefresh->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnRefreshClick ) );
	this->Connect( menuFileExit->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnExitClick ) );
	this->Connect( menuViewZoomIn->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnZoomInClick ) );
	this->Connect( menuViewZoomOut->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnZoomOutClick ) );
	this->Connect( m_submenuColorsInvert->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnInvertClick ) );
	this->Connect( m_submenuColorsMonoGray->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnMonoGrayClick ) );
	this->Connect( m_submenuColorsMonoRed->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnMonoRedClick ) );
	this->Connect( m_submenuColorsMonoBlue->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnMonoBlueClick ) );
	this->Connect( m_submenuColorsMonoGreen->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnMonoGreenClick ) );
	this->Connect( m_submenuColorColorize->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnColorizeClick ) );
	this->Connect( m_submenuColorTreshold->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnTreshClick ) );
	this->Connect( m_submenuColorSepia->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnSepiaClick ) );
	this->Connect( m_submenuTransformRotateLeft->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnRotateLeftClick ) );
	this->Connect( m_submenuTransformRotateRight->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnRotateRightClick ) );
	this->Connect( m_submenuTransformMirrorHorizontaly->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnMirrorHorizontalClick ) );
	this->Connect( m_submenuTransformMirrorVerticaly->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnMirrorVerticalClick ) );
	this->Connect( m_subMenuFiltersFocus->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnFocusClick ) );
	this->Connect( m_subMenuFiltersBlur->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnBlurClick ) );
	this->Connect( m_subMenuFiltersEdgeEnhacement->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnEdgeEnhacementClick ) );
	this->Connect( m_subMenuFiltersEdgeDetection->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnEdgeDetectionClick ) );
	this->Connect( m_subMenuFiltersRelief->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnReliefClick ) );
	this->Connect( m_subMenuFiltersColorRelief->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnColorReliefClick ) );
	this->Connect( m_subMenuFiltersLaplaceSharpening->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnLaplaceSharpeningClick ) );
	this->Connect( m_subMenuFiltersPrewitSharpening->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnPrewitSharpeningClick ) );
	this->Connect( m_subMenuFiltersSobelSharpening->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnSobelSharpeningClick ) );
	this->Connect( m_subMenuFiltersInverseLaplaceSharpening->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnInverseLaplaceSharpeningClick ) );
	this->Connect( m_subMenuFiltersConvolutionMatrix->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnConvolutionClick ) );
	this->Connect( menuEditSteganography->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnSteganographyClick ) );
	this->Connect( menuHelpAbout->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnAboutClick ) );
	this->Connect( m_toolOpen->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( MainFrameBase::OnOpenClick ) );
	this->Connect( m_toolSave->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( MainFrameBase::OnSaveClick ) );
	this->Connect( m_toolSaveAs->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( MainFrameBase::OnSaveAsClick ) );
	this->Connect( m_toolRefresh->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( MainFrameBase::OnRefreshClick ) );
	this->Connect( m_toolRotateL->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( MainFrameBase::OnRotateLeftClick ) );
	this->Connect( m_toolRotateR->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( MainFrameBase::OnRotateRightClick ) );
	this->Connect( m_toolMirrorV->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( MainFrameBase::OnMirrorVerticalClick ) );
	this->Connect( m_toolMirrorH->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( MainFrameBase::OnMirrorHorizontalClick ) );
	this->Connect( m_toolInverse->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( MainFrameBase::OnInvertClick ) );
	this->Connect( m_toolColorize->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( MainFrameBase::OnColorizeClick ) );
	this->Connect( m_toolSteganography->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnSteganographyClick ));
	m_scaleSlider->Connect( wxEVT_SCROLL_TOP, wxScrollEventHandler( MainFrameBase::OnSliderSlide ), NULL, this );
	m_scaleSlider->Connect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( MainFrameBase::OnSliderSlide ), NULL, this );
	m_scaleSlider->Connect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( MainFrameBase::OnSliderSlide ), NULL, this );
	m_scaleSlider->Connect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderSlide ), NULL, this );
	m_scaleSlider->Connect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( MainFrameBase::OnSliderSlide ), NULL, this );
	m_scaleSlider->Connect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderSlide ), NULL, this );
	m_scaleSlider->Connect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( MainFrameBase::OnSliderSlide ), NULL, this );
	m_scaleSlider->Connect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( MainFrameBase::OnSliderSlide ), NULL, this );
	m_scaleSlider->Connect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( MainFrameBase::OnSliderSlide ), NULL, this );
	canvas->Connect( wxEVT_PAINT, wxPaintEventHandler( MainFrameBase::OnPaint ), NULL, this );
	canvas->Connect( wxEVT_SIZE, wxSizeEventHandler( MainFrameBase::OnSize ), NULL, this );
}

MainFrameBase::~MainFrameBase()
{
	// Disconnect Events
	this->Disconnect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( MainFrameBase::OnCloseFrame ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnOpenClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnSaveClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnSaveAsClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnRefreshClick ) );
	this->Disconnect( wxID_EXIT, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnExitClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnZoomInClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnZoomOutClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnInvertClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnMonoGrayClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnMonoRedClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnMonoBlueClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnMonoGreenClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnColorizeClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnTreshClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnSepiaClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnRotateLeftClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnRotateRightClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnMirrorHorizontalClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnMirrorVerticalClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnFocusClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnBlurClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnEdgeEnhacementClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnEdgeDetectionClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnReliefClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnColorReliefClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnLaplaceSharpeningClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnPrewitSharpeningClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnSobelSharpeningClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnInverseLaplaceSharpeningClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnConvolutionClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnSteganographyClick ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnAboutClick ) );
	this->Disconnect( m_toolOpen->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( MainFrameBase::OnOpenClick ) );
	this->Disconnect( m_toolSave->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( MainFrameBase::OnSaveClick ) );
	this->Disconnect( m_toolSaveAs->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( MainFrameBase::OnSaveAsClick ) );
	this->Disconnect( m_toolRefresh->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( MainFrameBase::OnRefreshClick ) );
	this->Disconnect( m_toolRotateL->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( MainFrameBase::OnRotateLeftClick ) );
	this->Disconnect( m_toolRotateR->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( MainFrameBase::OnRotateRightClick ) );
	this->Disconnect( m_toolMirrorV->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( MainFrameBase::OnMirrorVerticalClick ) );
	this->Disconnect( m_toolMirrorH->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( MainFrameBase::OnMirrorHorizontalClick ) );
	this->Disconnect( m_toolInverse->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( MainFrameBase::OnInvertClick ) );
	this->Disconnect( m_toolColorize->GetId(), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( MainFrameBase::OnColorizeClick ) );
	this->Disconnect( m_toolSteganography->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( MainFrameBase::OnSteganographyClick ));
	m_scaleSlider->Disconnect( wxEVT_SCROLL_TOP, wxScrollEventHandler( MainFrameBase::OnSliderSlide ), NULL, this );
	m_scaleSlider->Disconnect( wxEVT_SCROLL_BOTTOM, wxScrollEventHandler( MainFrameBase::OnSliderSlide ), NULL, this );
	m_scaleSlider->Disconnect( wxEVT_SCROLL_LINEUP, wxScrollEventHandler( MainFrameBase::OnSliderSlide ), NULL, this );
	m_scaleSlider->Disconnect( wxEVT_SCROLL_LINEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderSlide ), NULL, this );
	m_scaleSlider->Disconnect( wxEVT_SCROLL_PAGEUP, wxScrollEventHandler( MainFrameBase::OnSliderSlide ), NULL, this );
	m_scaleSlider->Disconnect( wxEVT_SCROLL_PAGEDOWN, wxScrollEventHandler( MainFrameBase::OnSliderSlide ), NULL, this );
	m_scaleSlider->Disconnect( wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler( MainFrameBase::OnSliderSlide ), NULL, this );
	m_scaleSlider->Disconnect( wxEVT_SCROLL_THUMBRELEASE, wxScrollEventHandler( MainFrameBase::OnSliderSlide ), NULL, this );
	m_scaleSlider->Disconnect( wxEVT_SCROLL_CHANGED, wxScrollEventHandler( MainFrameBase::OnSliderSlide ), NULL, this );
	canvas->Disconnect( wxEVT_PAINT, wxPaintEventHandler( MainFrameBase::OnPaint ), NULL, this );
	canvas->Disconnect( wxEVT_SIZE, wxSizeEventHandler( MainFrameBase::OnSize ), NULL, this );
	
}

ConvolutionFrame::ConvolutionFrame( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	wxGridSizer* gSizer1;
	gSizer1 = new wxGridSizer( 4, 3, 0, 0 );
	
	m_kernel00 = new wxTextCtrl( this, wxID_ANY, wxT("0"), wxDefaultPosition, wxDefaultSize, 0 );
	gSizer1->Add( m_kernel00, 0, wxALL, 5 );
	
	m_kernel01 = new wxTextCtrl( this, wxID_ANY, wxT("0"), wxDefaultPosition, wxDefaultSize, 0 );
	gSizer1->Add( m_kernel01, 0, wxALL, 5 );
	
	m_kernel02 = new wxTextCtrl( this, wxID_ANY, wxT("0"), wxDefaultPosition, wxDefaultSize, 0 );
	gSizer1->Add( m_kernel02, 0, wxALL, 5 );
	
	m_kernel10 = new wxTextCtrl( this, wxID_ANY, wxT("0"), wxDefaultPosition, wxDefaultSize, 0 );
	gSizer1->Add( m_kernel10, 0, wxALL, 5 );
	
	m_kernel11 = new wxTextCtrl( this, wxID_ANY, wxT("0"), wxDefaultPosition, wxDefaultSize, 0 );
	gSizer1->Add( m_kernel11, 0, wxALL, 5 );
	
	m_kernel12 = new wxTextCtrl( this, wxID_ANY, wxT("0"), wxDefaultPosition, wxDefaultSize, 0 );
	gSizer1->Add( m_kernel12, 0, wxALL, 5 );
	
	m_kernel20 = new wxTextCtrl( this, wxID_ANY, wxT("0"), wxDefaultPosition, wxDefaultSize, 0 );
	gSizer1->Add( m_kernel20, 0, wxALL, 5 );
	
	m_kernel21 = new wxTextCtrl( this, wxID_ANY, wxT("0"), wxDefaultPosition, wxDefaultSize, 0 );
	gSizer1->Add( m_kernel21, 0, wxALL, 5 );
	
	m_kernel22 = new wxTextCtrl( this, wxID_ANY, wxT("0"), wxDefaultPosition, wxDefaultSize, 0 );
	gSizer1->Add( m_kernel22, 0, wxALL, 5 );
	
	m_divider = new wxTextCtrl( this, wxID_ANY, wxT("1"), wxDefaultPosition, wxDefaultSize, 0 );
	gSizer1->Add( m_divider, 0, wxALL, 5 );
	
	m_buttonOk = new wxButton( this, wxID_ANY, wxT("Ok"), wxDefaultPosition, wxDefaultSize, 0 );
	gSizer1->Add( m_buttonOk, 0, wxALL, 5 );
	
	m_buttonCancel = new wxButton( this, wxID_ANY, wxT("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
	gSizer1->Add( m_buttonCancel, 0, wxALL, 5 );
	
	
	this->SetSizer( gSizer1 );
	this->Layout();
	gSizer1->Fit( this );
	
	this->Centre( wxBOTH );
	
	// Connect Events
	m_buttonOk->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ConvolutionFrame::OnOkClick ), NULL, this );
	m_buttonCancel->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ConvolutionFrame::OnCancelClick ), NULL, this );
}

ConvolutionFrame::~ConvolutionFrame()
{
	// Disconnect Events
	m_buttonOk->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ConvolutionFrame::OnOkClick ), NULL, this );
	m_buttonCancel->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( ConvolutionFrame::OnCancelClick ), NULL, this );
	
}
