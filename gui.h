///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun  6 2014)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __GUI_H__
#define __GUI_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/menu.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/stattext.h>
#include <wx/slider.h>
#include <wx/toolbar.h>
#include <wx/scrolwin.h>
#include <wx/sizer.h>
#include <wx/propgrid/propgrid.h>
#include <wx/propgrid/advprops.h>
#include <wx/statusbr.h>
#include <wx/frame.h>
#include <wx/textctrl.h>
#include <wx/valgen.h>
#include <wx/button.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class MainFrameBase
///////////////////////////////////////////////////////////////////////////////
class MainFrameBase : public wxFrame 
{
	private:
	
	protected:
		wxMenuBar* m_menuBar;
		wxMenu* m_menuFile;
		wxMenu* m_menuView;
		wxMenu* m_menuEdit;
		wxMenu* m_subMenuColors;
		wxMenu* m_subMenuTranform;
		wxMenu* m_subMenuFilters;
		wxMenu* m_menuHelp;
		wxToolBar* m_toolBar1;
		wxToolBarToolBase* m_toolOpen; 
		wxToolBarToolBase* m_toolSave; 
		wxToolBarToolBase* m_toolSaveAs; 
		wxToolBarToolBase* m_toolRefresh; 
		wxToolBarToolBase* m_toolRotateL; 
		wxToolBarToolBase* m_toolRotateR; 
		wxToolBarToolBase* m_toolMirrorV; 
		wxToolBarToolBase* m_toolMirrorH; 
		wxToolBarToolBase* m_toolInverse; 
		wxToolBarToolBase* m_toolColorize; 
		wxToolBarToolBase* m_toolSteganography; 
		wxStaticText* m_staticText1;
		wxSlider* m_scaleSlider;
		wxStaticText* m_zoomLVL;
		wxBoxSizer* bSizer2;
		wxScrolledWindow* canvas;
		wxPropertyGrid* properties;
		wxStatusBar* m_statusBar;
		
		// Virtual event handlers, overide them in your derived class
		virtual void OnCloseFrame( wxCloseEvent& event ) { event.Skip(); }
		virtual void OnOpenClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSaveClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSaveAsClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnRefreshClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnExitClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnZoomInClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnZoomOutClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnInvertClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnMonoGrayClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnMonoRedClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnMonoBlueClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnMonoGreenClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnColorizeClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnTreshClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSepiaClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnRotateLeftClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnRotateRightClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnMirrorHorizontalClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnMirrorVerticalClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnFocusClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnBlurClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnEdgeEnhacementClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnEdgeDetectionClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnReliefClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnColorReliefClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnLaplaceSharpeningClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnPrewitSharpeningClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSobelSharpeningClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnInverseLaplaceSharpeningClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnConvolutionClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSteganographyClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnAboutClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSliderSlide( wxScrollEvent& event ) { event.Skip(); }
		virtual void OnPaint( wxPaintEvent& event ) { event.Skip(); }
		virtual void OnSize( wxSizeEvent& event ) { event.Skip(); }
		
	
	public:
		
		MainFrameBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Jednoduchý prohlížeč obrázků"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 913,300 ), long style = wxCLOSE_BOX|wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );
		
		~MainFrameBase();
	
};

///////////////////////////////////////////////////////////////////////////////
/// Class ConvolutionFrame
///////////////////////////////////////////////////////////////////////////////
class ConvolutionFrame : public wxDialog 
{
	private:
	
	protected:
		wxTextCtrl* m_kernel00;
		wxTextCtrl* m_kernel01;
		wxTextCtrl* m_kernel02;
		wxTextCtrl* m_kernel10;
		wxTextCtrl* m_kernel11;
		wxTextCtrl* m_kernel12;
		wxTextCtrl* m_kernel20;
		wxTextCtrl* m_kernel21;
		wxTextCtrl* m_kernel22;
		wxTextCtrl* m_divider;
		wxButton* m_buttonOk;
		wxButton* m_buttonCancel;
		
		// Virtual event handlers, overide them in your derived class
		virtual void OnOkClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCancelClick( wxCommandEvent& event ) { event.Skip(); }
		
	
	public:
		
		ConvolutionFrame( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Konvoluční matice"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE ); 
		~ConvolutionFrame();
	
};

#endif //__GUI_H__
