#ifndef PCX_H
#define PCX_H

#include "bitmapi.h" // Base class: BitmapI
#include "defines.h"
#include <map>

#define MAX_DECODE_SIZE 0x3f

typedef struct {
	uint8_t password;
	uint8_t version;
	uint8_t coding;
	uint16_t Xmin;
	uint16_t Ymin;
	uint16_t Xmax;
	uint16_t Ymax;
	uint16_t HDpi;
	uint16_t VDpi;
	uint16_t BPL;
	uint16_t paleteInfo;
} PCX_FILE_HEADER_T;

using namespace std;

class PCX : public BitmapI
{
public:
	virtual
	vector<unsigned char>* GetBitmapData();
	virtual
	bool SaveBitmap( wxString path , vector<unsigned char> * data, BITMAP_INNER_STATE_STRUCT_T state, BITMAP_DIMENSIONS_T dimens );
	virtual
	bool CanSteganography();
	virtual
	void ConvertSteganography();
	virtual bool SaveBitmap();
	PCX();
	virtual ~PCX();
	virtual void GetImageData();
	virtual void GetProperties( wxPropertyGrid* properties );
	virtual bool SaveBitmap( wxString path );

private:
	PCX_FILE_HEADER_T header;

	uint16_t GetPalete( unsigned char * from );
	uint8_t DecodeData( unsigned char * dest, unsigned char * source );
	uint32_t EncodeData( char * dest, unsigned char * source );
	uint32_t GeneratePalet( map<uint32_t, uint16_t> *paleta );
};

#endif // PCX_H
