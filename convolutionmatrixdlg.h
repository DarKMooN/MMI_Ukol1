#ifndef CONVOLUTIONMATRIXDLG_H
#define CONVOLUTIONMATRIXDLG_H

#include "gui.h" // Base class: ConvolutionFrame
#include "defines.h"

class ConvolutionMatrixDlg : public ConvolutionFrame
{
private:
	KERNEL_T *_kernel;
public:
	ConvolutionMatrixDlg( wxWindow *parent );
	void SetKernel( KERNEL_T *kernel );
	virtual ~ConvolutionMatrixDlg();

public:
	virtual
	void OnCancelClick( wxCommandEvent& event );
	virtual
	void OnOkClick( wxCommandEvent& event );
};

#endif // CONVOLUTIONMATRIXDLG_H
