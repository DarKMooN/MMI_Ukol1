#include "bmp.h"

BMP::BMP()
{
}
BMP::BMP( wxString filePath, bool loadFile )
{
	path = filePath;
	if( loadFile )
		LoadImage();
}

BMP::BMP( wxString filePath, wxClientDC *dc )
{
	path = filePath;
	if( LoadImage() ) {
		GetImageData();
		DrawBitmap( dc );
	}
}

BMP::BMP( wxString filePath, wxClientDC *dc, wxPropertyGrid *properties )
{
	path = filePath;
	if( LoadImage() ) {
		GetImageData();
		DrawBitmap( dc );
		GetProperties( properties );
	}
}

BMP::~BMP()
{
}

void BMP::GetImageData()
{
	header = {
		.type = BMP_TYPE,
		.size = MAKE32( file, 2 ),
		.reserved1 = MAKE16( file, 6 ),
		.reserved2 = MAKE16( file, 8 ),
		.startImageData = MAKE32( file, 10 )
	};
	fileInfo = {
		.size = MAKE32( file, 14 ),
		.width = MAKE32( file, 18 ),
		.height = MAKE32( file, 22 ),
		.planes = MAKE16( file, 26 ),
		.compression = MAKE32( file, 30 ),
		.imageSize = MAKE32( file, 34 ),
		.hRes = MAKE32( file, 38 ),
		.vRes = MAKE32( file, 42 ),
		.colorsUsed = MAKE32( file, 46 ),
		.colorsImportant = MAKE32( file, 50 )
	};

	dimensions.width = fileInfo.width;
	dimensions.height = fileInfo.height;
	originalDimensions.height = dimensions.height;
	originalDimensions.width = dimensions.width;

	stateStruct.bpp = MAKE16( file, 28 );

	// naplnění palety
	palete.clear();
	palete.reserve( pow( 2, stateStruct.bpp ) );
	for( uint32_t i = 0x36; i < header.startImageData; i += 4 ) {
		uint32_t rgb = MAKE32( file, i ) & 0xffffff;
		palete.push_back( wxColour( rgb ) );
	}

	// mask - maska pro odfiltrování nepotřebných bitů
	// shift - po kolika bitech se bude měnit pozice bitů
	// bitStart - kde se má začít s offstem bitů při filtrování
	int mask, shift, bitStart;
	switch( stateStruct.bpp ) {
	case 1:
		bitStart = 7;
		stateStruct.validBytes = fileInfo.width / 8;
		mask = 0x1;
		shift = 1;
		break;
	case 4:
		stateStruct.validBytes = fileInfo.width / 2;
		mask = 0xf;
		shift = 4;
		bitStart = 4;
		break;
	case 8:
		stateStruct.validBytes = fileInfo.width;
		shift = 8;
		bitStart = 0;
		mask = 0xff;
		break;
	case 24:
		stateStruct.validBytes = fileInfo.width * 3;
		shift = 0;
		bitStart = -1;
		mask = 0xff;
		break;
	default:
		bitStart = -1;
		stateStruct.validBytes = mask = shift = 0;
		break;
	}

	// průchod dat po line
	stateStruct.line = ceil( ( dimensions.width * stateStruct.bpp / ( double ) 32 ) ) * 4;


	// alokování potředného místa pro data bitmapy ( h * v * 3) a alokace místa pro scan line
	unsigned char * data = ( unsigned char * ) malloc( fileInfo.height * fileInfo.width * 3 * sizeof( unsigned char ) );
	unsigned char * tempData = ( unsigned char * ) malloc( stateStruct.line * sizeof( unsigned char ) );

	uint32_t actHeight = 0;
	stateStruct.dataIndex = 0;

	// scan line do výšky obrázku
	uint32_t tempPointer = stateStruct.line * fileInfo.height + header.startImageData - stateStruct.line;
	for( uint32_t i = tempPointer; i >= header.startImageData && actHeight < dimensions.height; i -= stateStruct.line, actHeight++ ) {
		// kopírování řádku do paměti
		memcpy( tempData, &file[i], stateStruct.line );
		// oříznutí nepotřebných dat v řádku
		for( uint32_t j = 0, actWidth = 0; j <= stateStruct.validBytes; j++ ) {
			if( stateStruct.bpp == 24 && actWidth < dimensions.width ) {
				data[stateStruct.dataIndex++] = tempData[j+2];
				data[stateStruct.dataIndex++] = tempData[j+1];
				data[stateStruct.dataIndex++] = tempData[j];
				j += 2;
				actWidth++;
			}
			// vymaskování bitů podle bpp (pro bpp = 1 musí být 8 průchodů, pro bpp = 4 musí být 2 průchody, pro bpp = 8 musí být 1 průchod. bpp = 24 má zvlášní handler)
			// actWidth ukončí procházení poté, co se dosáhne šířky
			for( int bits = bitStart ; bits >= 0 && actWidth < dimensions.width; bits -= shift, actWidth++ ) {
				unsigned char index = ( tempData[j] >> bits ) & mask;
				data[stateStruct.dataIndex++] = palete[ index ].Blue();
				data[stateStruct.dataIndex++] = palete[ index ].Green();
				data[stateStruct.dataIndex++] = palete[ index ].Red();
				//cout << ( index == 0 ? "*" : "x" );
			}
		}
		//cout << endl;
	}
	bitmapValidData.clear();
	bitmapValidData.reserve( stateStruct.dataIndex );
	bitmapValidData.resize( stateStruct.dataIndex );
	memcpy( &bitmapValidData[0], data, stateStruct.dataIndex );
	free( tempData );
	free( data );
}

void BMP::GetProperties( wxPropertyGrid* properties )
{
	properties->Clear();
	properties->Append( new wxIntProperty( wxT( "Výška" ), "Height", dimensions.height ) );
	properties->Append( new wxIntProperty( wxT( "Šířka" ), "Width", dimensions.width ) );
	properties->Append( new wxIntProperty( wxT( "Bpp" ), "Bpp", stateStruct.bpp ) );
	properties->Append( new wxIntProperty( wxT( "Velikost" ), "Size", header.size ) );
	properties->Append( new wxIntProperty( wxT( "Velikost palety" ), "PaletSize", header.startImageData - 0x36 ) );
	properties->Append( new wxIntProperty( wxT( "Velikost obrazových dat" ), "ImageDataSize", stateStruct.dataIndex ) );
	properties->Append( new wxIntProperty( wxT( "DPI" ), "Dpi", ceil( fileInfo.hRes / 100 * 2.54 ) ) );
	properties->Append( new wxIntProperty( wxT( "hRes" ), "hres", fileInfo.hRes ) );
	properties->Append( new wxIntProperty( wxT( "vRes" ), "vres", fileInfo.vRes ) );
	properties->Append( new wxIntProperty( wxT( "Line" ), "Line", stateStruct.line ) );
	properties->Append( new wxIntProperty( wxT( "VB" ), "VB", stateStruct.validBytes ) );
}

bool BMP::SaveBitmap( wxString path )
{
	ofstream ofile( path, ios::out | ios::trunc | ios::binary );
	if( ofile.is_open() ) {

		map<uint32_t, uint16_t> paleta;

		vector<char> tempVector;
		uint32_t validBytes = dimensions.width * 3, paletSize = GeneratePalet( &paleta );

		uint32_t line = ceil( ( dimensions.width * stateStruct.bpp / ( double ) 32 ) ) * 4;
		uint32_t dataSize = line * dimensions.height;
		uint8_t startBit, divider;
		switch( stateStruct.bpp ) {
		case 1:
			startBit = 7;
			divider = 24;
			break;
		case 4:
			startBit = 4;
			divider = 6;
			break;
		case 8:
			startBit = 0;
			divider = 3;
			break;
		case 24:
			break;
		}

		tempVector.reserve( BMP_HEADER_SIZE + paletSize + dataSize );
		char * tempVectorDataPtr = tempVector.data();

		// vytvoření hlavičky souboru
		BigEndian( ( uint16_t )BMP_TYPE, &tempVectorDataPtr ); // identifikátor BMP
		BigEndian( ( uint32_t )BMP_HEADER_SIZE + paletSize + dataSize, &tempVectorDataPtr ); // velikost souboru
		BigEndian( ( uint32_t )0, &tempVectorDataPtr );// reserved 2*2B
		BigEndian( ( uint32_t )0x36 + paletSize, &tempVectorDataPtr ); // offset dat obrázku - 54+paleta
		BigEndian( ( uint32_t )0x28, &tempVectorDataPtr ); // velikost bitmapinfoheader (40B)
		BigEndian( ( uint32_t )dimensions.width, &tempVectorDataPtr ); // šířka
		BigEndian( ( uint32_t )dimensions.height, &tempVectorDataPtr ); // výška
		BigEndian( ( uint16_t )1, &tempVectorDataPtr ); // roviny = 1
		BigEndian( ( uint16_t )stateStruct.bpp, &tempVectorDataPtr ); // bpp
		BigEndian( ( uint32_t )0, &tempVectorDataPtr ); // komprese = 0
		BigEndian( ( uint32_t )dataSize, &tempVectorDataPtr ); // velikost obrazových dat
		BigEndian( ( uint32_t )0, &tempVectorDataPtr ); // hRes
		BigEndian( ( uint32_t )0, &tempVectorDataPtr ); // vRes
		BigEndian( ( uint32_t )0, &tempVectorDataPtr ); // barev v obrázku || 0
		BigEndian( ( uint32_t )0, &tempVectorDataPtr ); // důležité barvy || 0

		if( stateStruct.bpp != 24 ) {
			map<uint16_t, uint32_t> tempPaletMap;
			// vypsání palety barev
			for( pair<uint32_t, uint16_t> i : paleta ) {
				tempPaletMap.insert( pair<uint16_t, uint32_t>( i.second, i.first ) );
			}
			// vypsání palety ve správném pořadí
			for( pair<uint16_t, uint32_t> i : tempPaletMap )
				BigEndian( i.second, &tempVectorDataPtr );
		}

		for( uint32_t i = stateStruct.dataIndex - validBytes, actHeight = 0; i >= 0 && actHeight < dimensions.height; i -= validBytes, actHeight++ ) {
			// Ukládání 24b obrázku
			if( stateStruct.bpp == 24 ) {
				memcpy( tempVectorDataPtr, &bitmapValidData[i], validBytes );
				// prohození R a B složky ve zkopírovaných datech
				for( uint32_t j = 0; j < validBytes / 3; j++ ) {
					char temp = *( tempVectorDataPtr + j * 3 );
					*( tempVectorDataPtr + j * 3 ) = *( tempVectorDataPtr + 2 + j * 3 );
					*( tempVectorDataPtr + 2 + j * 3 ) = temp;
				}
				// opravení ukazatele
				tempVectorDataPtr += validBytes;
				//doplnění řádku nulami
				memset( tempVectorDataPtr, 0, line - validBytes );
				// opravení ukazatele
				tempVectorDataPtr += line - validBytes;
			} else {
				int8_t bits = startBit;
				*tempVectorDataPtr = 0;
				// přečtení a naplnění vektoru pro výstup
				for( uint32_t j = 0, actWidth = 0; j < validBytes && actWidth < dimensions.width; j += 3, actWidth++ ) {
					// vytvoření barvy
					uint32_t color = ( bitmapValidData[i+j] << 16 ) | ( bitmapValidData[i+j+1] << 8 ) | bitmapValidData[i+j+2];
					// vyhledání indexu barvy v paletě
					uint16_t colorIndex = paleta[color];
					*tempVectorDataPtr |= colorIndex << bits;
					if( bits == 0 || stateStruct.bpp == 8 ) {
						*++tempVectorDataPtr = 0;
						bits = startBit;
					} else
						bits -= stateStruct.bpp;
				}
				// doplnění do scanline
				for( uint32_t j = validBytes / divider; j < line; j++ ) {
					*++tempVectorDataPtr = 0;
				}
			}
		}
		ofile.write( tempVector.data(), BMP_HEADER_SIZE + paletSize + dataSize );
		ofile.close();
		return true;
	}
	return false;
}

// vrátí výslednou velikost palety
uint32_t BMP::GeneratePalet( map<uint32_t, uint16_t> *paleta )
{
	if( stateStruct.bpp == 24 )
		return 0;
	return BitmapI::GeneratePalet( paleta ) * 4;
}
bool BMP::SaveBitmap()
{
	return SaveBitmap( path );
}

bool BMP::CanSteganography()
{
	return stateStruct.bpp == 24 ? true : false;
}

void BMP::ConvertSteganography()
{
	stateStruct.bpp = 24;
}

bool BMP::SaveBitmap( wxString path , vector<unsigned char> * data, BITMAP_INNER_STATE_STRUCT_T state, BITMAP_DIMENSIONS_T dimens )
{
	bitmapValidData = *data;
	stateStruct = state;
	dimensions = dimens;
	return SaveBitmap( path );
}
vector<unsigned char>* BMP::GetBitmapData()
{
	stateStruct.colorPlanes = stateStruct.bpp == 24 ? 3 : 1;
	stateStruct.bpp = stateStruct.bpp == 24 ? 8 : stateStruct.bpp;
	return &bitmapValidData;
}
