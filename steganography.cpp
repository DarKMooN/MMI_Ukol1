#include "steganography.h"

Steganography::Steganography()
{
}

Steganography::~Steganography()
{
	delete _data;
}

uint32_t Steganography::GetDWord( uint32_t index )
{
	uint32_t lenght = 0;
	for( uint32_t i = index; i < index + 32; i++ ) {
		lenght = ( _data->at( i ) & 0x1 ) | ( lenght << 1 );
	}
	return lenght;
}

bool Steganography::GetHeader()
{
	if( _data->size() < 240 )
		return false;

	header.size = GetDWord( 0 );
	header.crc = GetDWord( 32 );
	header.compress = GetByte( 224 );
	header.crypto = GetByte( 232 );
	GetString( 64, header.name, 20 );
	return true;
}

string Steganography::GetSteganography( vector<unsigned char> * data )
{
	SwapRB( _data, data );

	if( GetHeader() ) {
		if( header.compress != 0 || header.crypto != 0 || header.size > ( _data->size() - 240 ) / 8 )
			return "";
		wxString retVal = "";
		char ch;
		for( uint32_t i = 240; i < 240 + header.size * 8; i += 8 ) {
			ch = GetByte( i );
			retVal.Append( ch );
		}
		uint32_t crc = GetCRC( retVal.ToStdString() );
		if( crc == header.crc )
			return retVal.ToStdString();
		else
			return "";
	}
	return "";
}

uint8_t Steganography::GetByte( uint32_t index )
{
	uint8_t lenght = 0;
	for( uint32_t i = index; i < index + 8; i++ ) {
		lenght = ( _data->at( i ) & 0x1 ) | ( lenght << 1 );
	}
	return lenght;
}

void Steganography::GetString( uint32_t index, char * dest, uint32_t length )
{
	for( uint32_t i = index, c = 0; i < index + length; i++, c++ ) {
		*dest++ = ( char )GetByte( index + c * 8 );
	}
	*dest = '\0';
}

uint32_t Steganography::GetCRC( string input )
{
	uint32_t crc = 0, pos = 0;
	crc = crc ^ ~0U;
	uint32_t size = input.length();
	while ( size-- )
		crc = crc32_tab[( crc ^ input[pos++] ) & 0xFF] ^ ( crc >> 8 );
	return crc ^ ~0U;
}

void Steganography::SetByte( uint32_t index, char input )
{
	for( int8_t bit = 7; bit >= 0; --bit, ++index ) {
		int b = ( input >> bit ) & 1;
		_data->at( index ) = ( ( _data->at( index ) & 0xFE ) | b );
	}
}

void Steganography::SetDWord( uint32_t index, uint32_t input )
{
	for( int8_t bit = 31; bit >= 0; --bit, ++index ) {
		int b = ( input >> bit ) & 1;
		_data->at( index ) = ( ( _data->at( index ) & 0xFFFFFFFE ) | b );
	}
}

void Steganography::SetHeader( uint32_t crc, uint32_t size )
{
	string str( "?" );
	SetDWord( 0, size );
	SetDWord( 32, crc );
	SetString( 64, str, 19, 0 );
	SetByte( 224, 0 );
	SetByte( 232, 0 );
}

bool Steganography::SetSteganography( string input, vector<unsigned char> * data )
{
	uint32_t size = data->size();
	if( ( input.length() + 30 ) * 8 > size )
		return false;
	SwapRB( _data, data );
	uint32_t crc = GetCRC( input );
	SetHeader( crc, input.length() );
	SetString( 240, input );
	SwapRB( data, _data );
	return true;
}

void Steganography::SwapRB( vector<unsigned char> * dest, vector<unsigned char> * source )
{
	dest->clear();
	dest->reserve( source->size() );
	for( uint32_t counter = 0; counter < source->size(); counter += 3 ) {
		dest->push_back( source->at( counter + 2 ) );
		dest->push_back( source->at( counter + 1 ) );
		dest->push_back( source->at( counter ) );
	}
}

void Steganography::SetString( uint32_t index, string input, uint32_t padRight, char ch )
{
	uint32_t count = 0;
	for( uint32_t i = index; i < index + input.length(); i++, count++ ) {
		SetByte( index + count * 8, ( uint8_t )input[count] );
	}
	for( uint32_t i = index + input.length(); i < index + input.length() + padRight; i++, count++ ) {
		SetByte( index + count * 8, ( uint8_t )ch );
	}
}
