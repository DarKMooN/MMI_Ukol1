#ifndef DEFINES_H
#define DEFINES_H

#include <wx/wx.h>
#include <wx/filedlg.h>
#include <wx/propgrid/propgrid.h>
#include <algorithm>
#include <iostream>
#include <string>


#define MAKE16(var, index) ((uint16_t)(((unsigned char)(var[index + 1]) << 8 ) | ((unsigned char)var[index])))
#define MAKE32(var, index) ((uint32_t)(((unsigned char)(var[index + 3]) << 24 ) | \
                                       ((unsigned char)(var[index + 2]) << 16 ) | \
                                       ((unsigned char)(var[index + 1]) << 8 ) | \
                                       ((unsigned char)var[index])))

#define MAKE24(var, index) ((uint32_t)((((unsigned char)(var[index + 2]) << 16 ) | \
                                        ((unsigned char)(var[index + 1]) << 8 ) | \
                                        ((unsigned char)var[index]))) & 0xffffff

#define BMP_TYPE 0x4D42
#define PCX_TYPE 0xA

#define BMP_HEADER_SIZE 0x36
#define PCX_HEADER_SIZE 0x80

typedef enum { unknown, bmp, pcx } file_types_t;

typedef struct {
	double kernel[9];
	double divider;
} KERNEL_T;

#endif
